e3-rf_power_station archive file template
======

This is a template file to generate the `.archive` files. To get the archive file for `Spk-010RFC:RFS-RFPS-010` run
```
rfps=Spk-010RFC:RFS-RFPS-010 && msi -MP=${rfps},R=: -o${rfps/:/_}.archive rf_power_station.archive
```
